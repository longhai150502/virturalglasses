
let renderListGlasses = () => {
    return (document.getElementById("vglassesList").innerHTML = dataGlasses
        .map((element) => {
          let { id, src, virtualImg} = element;

          return `
          <div id=${id} class = "col col-sm-4 glass">
            <img onclick="thuKinh(${id})" src="${src}" alt="" />
          </div>
          `;
        })
        .join(""));
}
renderListGlasses(dataGlasses);
